function IKTapControllerDelegate()
{

	this.tapControllerDidReceivedEvent = function(tapEvent) {}

}

function IKTapEvent()
{
	this.x = null;
	this.y = null;
}

function IKTapController()
{

	this.delegate = null;

	this.initialize = function(canvas)
	{
		canvas.addEventListener("mouseup", globalTapController.touchUp, false);
		canvas.addEventListener("touchend", globalTapController.touchUp, false);
	}

	this.touchUp = function(event)
	{
		console.log("Touch up!");

		var tapEvent = new IKTapEvent();
		tapEvent.x = event.clientX;
		tapEvent.y = event.clientY;

		globalTapController.delegate.tapControllerDidReceivedEvent(this, tapEvent);
	}
}


function FSEIdentifiable()
{

	this.identifier = null;

}

function FSEMainControllerDelegate()
{

	this.mainControllerDidFinishEvent = function(controller, senderIdentifier) {}

}

function FSEIterable()
{

	this.step = function() {}

}

function FSEMainController()
{

	this.stateMachine = null;
	this.tapController = null;
	this.delegate = null;

	this.initialize = function(tapController)
	{

		this.tapController = tapController;

		this.stateMachine = new FSEStateMachine()
		this.stateMachine.initialize();

	}

	this.register = function(iterable, state)
	{
		this.stateMachine.register(iterable, state);
	}

	this.start = function(state)
	{

		this.didFinishEventSenderIdentfier = null;

		this.stateMachine.state = state;

		window.requestAnimationFrame(mainLoop);

	}

	this.step = function()
	{
		var iterable = this.stateMachine.iterable();

		if (iterable != null)
		{
			this.tapController.delegate = iterable;
			iterable.flowDelegate = this;

			iterable.step()

			if (this.didFinishEventSenderIdentfier == null)
			{
				window.requestAnimationFrame(mainLoop)
			}
			else
			{
				var didFinishEventSenderIdentfier = this.didFinishEventSenderIdentfier;
				this.delegate.mainControllerDidFinishEvent(this, didFinishEventSenderIdentfier);
			}
		}
		else
		{
			console.log("Bye-Bye!");
		}
	}

	this.didFinishEvent = function(sender)
	{

		this.didFinishEventSenderIdentfier = sender.identifier;

	}

}

function mainLoop() {

	globalMainController.step()

}

function FSEStateMachine()
{

	this.state = null;
	this.stateToIterableMap = null;

	this.initialize = function()
	{

		this.stateToIterableMap = {};

	}

	this.register = function(iterable, state)
	{
		this.stateToIterableMap[state] = iterable;
	}

	this.iterable = function()
	{
		return this.stateToIterableMap[this.state];;
	}
}

function FSEFlowDelegate()
{

	this.didFinishEvent = function(sender) {}

}

function CKImage()
{
	this.fullImage = null;

	this.initialize = function(fullImage)
	{
		this.fullImage = fullImage;
	}

	this.width = function()
	{

		return this.fullImage.width;

	}

	this.height = function()
	{

		return this.fullImage.height;

	}

	this.image = function()
	{
		return this.fullImage;
	}
}

function CKDrawable()
{
	this.drawableIdentifier = null;

	this.frame = null;
	this.image = null;
}

function CKStaticImage()
{

	this.drawableIdentifier = null;

	this.frame = null;
	this.image = null;

	this.initialize = function(image)
	{
		this.drawableIdentifier = window.crypto.getRandomValues(new Uint32Array(4)).join('-');

		this.image = new CKImage()
		this.image.initialize(image)

		this.frame = new CKFrame(0, 0, this.image.width(), this.image.height())
	}

}

function CKFrame()
{
	this.x = null;
	this.y = null;
	this.width = null;
	this.height = null;

	this.initialize = function(x, y, width, height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

}

function CKCanvas()
{

	this.canvas = null;
	this.canvasContext = null;

	this.initialize = function(canvas)
	{
		this.canvas = canvas;
		this.canvasContext = canvas.getContext('2d')
	}

	this.clear = function()
	{
		this.canvasContext.fillStyle = '#000000';
		this.canvasContext.fillRect(0, 0, canvas.width, canvas.height)
	}

	this.draw = function(drawable)
	{
		this.canvasContext.drawImage(drawable.image.image(), drawable.frame.x, drawable.frame.y)
	}

}

function CKRender()
{
	this.canvas = null;
	this.drawableObjects = null;

	this.initialize = function(canvas)
	{
		this.drawableObjects = [];
		this.canvas = canvas;
	}

	this.render = function()
	{
		var strongThis = this;

		this.canvas.clear()

		this.drawableObjects.forEach(function (drawableObject, index) {

			strongThis.canvas.draw(drawableObject)

		});
	}

	this.add = function(drawableObject)
	{
		this.drawableObjects.push(drawableObject)
	}
}

function RDCreditsControllerScreen()
{
	this.framesCounter = null;
	this.render = null;
	this.stateFlowDelegate = null;
	this.identifier = null;

	this.initialize = function(canvas, tapController)
	{
		this.identifier = "Credits Controller Screen";

		this.tapController = tapController;

		this.render = new CKRender()
		this.render.initialize(canvas)

		var demensdeumLogoImage = new Image();
		demensdeumLogoImage.src = "data/demensdeumLogo.png";

		var demensdeumLogo = new CKStaticImage();;
		demensdeumLogo.initialize(demensdeumLogoImage)

		this.render.add(demensdeumLogo);
	}

	this.step = function()
	{
		this.framesCounter += 1;

		if (this.framesCounter < 200)
		{
			this.render.render()
		}
		else
		{
			this.flowDelegate.didFinishEvent(this);
		}
	}

	this.tapControllerDidReceivedEvent = function(controller, tapEvent)
	{
		console.log("tapControllerDidReceivedEvent");

		this.flowDelegate.didFinishEvent(this);
	}

}

function RDDemoControllerScreen()
{
	this.backgroundFirst = null;
	this.backgroundSecond = null;
	this.render = null;

	this.initialize = function(canvas, tapController)
	{
		this.identifier = "Demo Controller Screen";

		this.tapController = tapController;

		this.render = new CKRender()
		this.render.initialize(canvas)

		var riseTextImage = new Image();
		riseTextImage.src = "data/rise.png";

		var eyesImage = new Image();
		eyesImage.src = "data/eyes.png";

		var phoenixImage = new Image();
		phoenixImage.src = "data/phoenix.png";

		var phoenix = new CKStaticImage();
		phoenix.initialize(phoenixImage)

		phoenix.frame.x = 100;
		phoenix.frame.y = 190;

		var eyesText = new CKStaticImage();
		eyesText.initialize(eyesImage)
		eyesText.frame.x = 30;
		eyesText.frame.y = 500;

		var riseText = new CKStaticImage();
		riseText.initialize(riseTextImage)

		var backgroundImage = new Image();
		backgroundImage.src = "data/background.png";

		backgroundFirst = new CKStaticImage();
		backgroundFirst.initialize(backgroundImage)

		backgroundSecond = new CKStaticImage();
		backgroundSecond.initialize(backgroundImage)

		backgroundSecond.frame.x = 320;

		this.render.add(backgroundFirst);
		this.render.add(backgroundSecond);
		this.render.add(riseText);
		this.render.add(eyesText);
		this.render.add(phoenix);
	}

	this.step = function()
	{
		this.render.render()

		backgroundFirst.frame.x -= 1;
		backgroundSecond.frame.x -= 1;

		if (backgroundFirst.frame.x <= -320)
		{
			backgroundFirst.frame.x = 0;
			backgroundSecond.frame.x = 320;
		}

	}

	this.tapControllerDidReceivedEvent = function(controller, tapEvent)
	{
		console.log("tapControllerDidReceivedEvent");
	}

}

function RDMainController()
{
	this.mainController = null;

	this.initialize = function(tapController)
	{
		var canvas = new CKCanvas();
		canvas.initialize(document.getElementById('canvas'))

		this.mainController = new FSEMainController();
		this.mainController.initialize(tapController);
		this.mainController.delegate = this;

		var creditsControllerScreen = new RDCreditsControllerScreen();
		creditsControllerScreen.initialize(canvas);
		this.mainController.register(creditsControllerScreen, "Credits Screen");

		var demoControllerScreen = new RDDemoControllerScreen();
		demoControllerScreen.initialize(canvas);
		this.mainController.register(demoControllerScreen, "Demo Screen");
	}

	this.start = function()
	{
		this.mainController.start("Credits Screen");
	}

	this.step = function()
	{
		this.mainController.step()
	}

	this.mainControllerDidFinishEvent = function(controller, senderIdentifier)
	{
		console.log("mainControllerDidFinishEvent");

		if (senderIdentifier == "Credits Controller Screen")
		{
			this.mainController.start("Demo Screen");
		}
	}
}

var main = function(arguments)
{
	globalTapController = new IKTapController()
	globalTapController.initialize(document.getElementById('canvas'))

	globalMainController = new RDMainController()
	globalMainController.initialize(globalTapController);
	globalMainController.start()
}

var globalMainController = null;
var globalTapController = null;


