wget \
     --recursive \
     --no-clobber \
     --page-requisites \
     --html-extension \
     --convert-links \
     --restrict-file-names=windows \
     --domains demensdeum.com \
     --no-parent \
         demensdeum.com/blog 
